import java.sql.Date;

public class User {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Date birthDate;

    public User(String email, String password, String firstName, String lastName, Date birthDate){
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword(){
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }
}
