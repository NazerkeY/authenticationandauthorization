import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import org.jasypt.util.password.StrongPasswordEncryptor;

import java.io.*;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class Portal {

    public Connection connection() throws ClassNotFoundException, IOException, SQLException {

        Class.forName("org.postgresql.Driver");

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("jasypt"); //secret key password

        Properties properties = new EncryptableProperties(encryptor); //to read the encrypted password in config.properties file
        InputStream inputStream = new FileInputStream("src/main/resources/config.properties");
        properties.load(inputStream);

        return DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("user"), properties.getProperty("password"));
    }

    public void createTable(){
        String sqlStatement = "CREATE TABLE IF NOT EXISTS users(id SERIAL NOT NULL PRIMARY KEY, " +
                "email VARCHAR(120) UNIQUE NOT NULL, password VARCHAR(120) NOT NULL,first_name VARCHAR(120), last_name VARCHAR(120), birth_date DATE)";

        try(Connection connection = connection();
        Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlStatement);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public long addUser(User user) {
        long id = 0;
        String sqlStatement = "INSERT INTO users(email, password, first_name, last_name, birth_date)" +
                " VALUES(?, ?, ?, ?, ?)";

        StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();

        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, encryptor.encryptPassword(user.getPassword()));
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setDate(5, user.getBirthDate());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        id = resultSet.getLong(1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void getInfo(String passEmail){
        String sqlStatement = "SELECT email, first_name, last_name, birth_date FROM users WHERE email = ?";
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setString(1, passEmail);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                if(!resultSet.next()){
                    System.out.println("No matches were found");
                }
                else{
                    do{
                        String email = resultSet.getString("email");
                        String firstName = resultSet.getString("first_name");
                        String lastName = resultSet.getString("last_name");
                        Date birthDate = resultSet.getDate("birth_date");

                        System.out.println("User: \n" +
                                "first name = " + firstName +
                                ", last name = " + lastName +
                                ", email = " + email +
                                ", Date of birth = " + birthDate);
                    } while(resultSet.next());
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(String passEmail){
        String sqlStatement = "DELETE FROM users WHERE email = ?";
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setString(1, passEmail);
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean checkUser(String passEmail, String passPassword){

        String sqlStatement = "SELECT email, password FROM users WHERE email = ?";
        boolean checked = false;
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, passEmail);

            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String encryptedStoredPassword = resultSet.getString("password");
                    StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
                    if(encryptor.checkPassword(passPassword, encryptedStoredPassword)){
                        checked = true;
                    }
                    else{
                        checked = false;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return checked;
    }

    public String getNameOfUser(String passEmail, String passPassword){

        String sqlStatement = "SELECT first_name, password FROM users WHERE email = ?";
        String name = "";
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, passEmail);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String encryptedStoredPassword = resultSet.getString("password");
                    StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor(); //to encrypt the password that user entered

                    if(encryptor.checkPassword(passPassword, encryptedStoredPassword)){
                        name = resultSet.getString("first_name");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return name;
    }
}
