import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Launch {
    private static final String regexForEmail = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
    private static final String regexForPassword = "^[A-Za-z0-9+_.$@#%-].{4,30}";

    public boolean isValid(String dateStr) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        simpleDateFormat.setLenient(false);
        try {
            simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public void launch() throws SQLException, IOException, ClassNotFoundException, ParseException {
        Portal portal = new Portal();
        System.out.println("Welcome! Please sign in to get information. (Enter 'Sign in')\n" +
                "If you don't have an account yet, then register (Enter 'Register')\n" +
                "Press 'Q' to exit.");

        portal.connection();
        portal.createTable();
        boolean logged = false;
        boolean processEmail = true;
        boolean processPassword = true;
        boolean processDate = true;
        String curEmail = "";

        while (true) {
            Scanner scanner = new Scanner(System.in);
            String option = scanner.nextLine();
            if (!option.equals("Q")) {
                if (option.equals("Sign in") && !logged) {
                    System.out.println("Enter your email:");
                    String email = scanner.next();
                    System.out.println("Enter your password:");
                    String password = scanner.next();

                    if (portal.checkUser(email, password)) {
                        logged = true;
                        curEmail = email;
                        System.out.println(String.format("Welcome %s ! If you want to exit, press 'Q'.\n" +
                                "If you want to get the information about user, enter 'Get'\n" +
                                "If you want to delete your account, enter 'Delete'", portal.getNameOfUser(email, password)));

                    } else {
                        System.out.println("Your password or email is NOT valid! Try again.");
                        logged = false;
                    }
                } else if (option.equals("Register")) {
                    System.out.println("Enter your information to sign up");
                    while(processEmail) {

                        System.out.println("Enter email:");
                        String email = scanner.nextLine();
                        Pattern patternEmail = Pattern.compile(regexForEmail);
                        Matcher matcherEmail = patternEmail.matcher(email);

                        if (matcherEmail.matches()) {
                            while (processPassword) {
                                System.out.println("Create your own password");
                                String password = scanner.nextLine();
                                Pattern patternPassword = Pattern.compile(regexForPassword);
                                Matcher matcherPassword = patternPassword.matcher(password);

                                if (matcherPassword.matches()) {
                                    System.out.println("Enter your name");
                                    String firstName = scanner.nextLine();

                                    System.out.println("Enter your surname");
                                    String lastName = scanner.nextLine();

                                    while(processDate) {
                                        System.out.println("Enter your date of birth in dd/MM/yyyy format:");
                                        String birthDate = scanner.next();

                                        if (portal.checkUser(email, password)) {
                                            System.out.println("Account exists");
                                            processDate = false;
                                            processEmail = false;
                                            processPassword = false;
                                        } else {
                                            if (isValid(birthDate)) {
                                                portal.addUser(new User(email, password, firstName, lastName, new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime())));
                                                System.out.println(String.format("You've been registered successfully, %s !\n" +
                                                        "If you want to exit, press 'Q'.\n" +
                                                        "If you want to get the information about user, enter 'Get'\n" +
                                                        "If you want to delete your account, enter 'Delete'", portal.getNameOfUser(email, password)));
                                                logged = true;
                                                curEmail = email;
                                                processDate = false;
                                                processEmail = false;
                                                processPassword = false;
                                            } else {
                                                System.out.println("Invalid format for date");
                                            }
                                        }
                                    }
                                } else {
                                    System.out.println("Your password length must be at least 5 characters");
                                }
                            }
                        }
                            else{
                                System.out.println("Invalid form for email");
                            }
                        }
                }
                else if(option.equals("Q")){
                    break;
                }
                else if (option.equals("Sign in") && logged) {
                    System.out.println("You've already logged in");
                }
                else if(option.equals("Get") && logged){
                    System.out.println("Enter an email of the user");
                    String passEmail = scanner.next();
                    portal.getInfo(passEmail);
                }
                else if(option.equals("Delete") && logged){
                    System.out.println("Write your email to confirm");
                    String email = scanner.next();

                    if(curEmail.equals(email)){
                        portal.deleteUser(email);
                        System.out.println("Your account was deleted successfully");
                        break;
                    }
                    else{
                        System.out.println("Email doesn't match");
                    }
                }
            }
            else{
                break;
            }
        }
    }
}
